#include<iostream>
using namespace std;

void printImage(int image[4][4])
{
    for(int i=0; i<4; ++i)
    {
        for(int j=0; j<4; ++j)
        {
            cout<<image[i][j]<<" ";
        }
        cout<<endl;
    }
}

void Counterclockwise90(int matrix[4][4])
{
    int a[4][4];
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
        a[3-j][i]=matrix[i][j];
        printImage(a);
}


int main()
{
    int a[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
    printImage(a);
    cout<<endl;
    Counterclockwise90(a);
    return 0;
}
