//
//  main.cpp
//  素数和.cpp
//
//  Created by 黄靖文 on 2018/6/6.
//  Copyright © 2018年 黄靖文. All rights reserved.
#include<stdio.h>
int main()
{
    int i;
    int j;
    int count=0;
    for(i=1;i<=1000;i++)
    {
        for(j=2;j<i;j++)
        {
            if(i%j==0)
                break;
        }
        if(j!=i)
            continue;
        count++;
        printf("%4d",i);
        if(count%5!=0)
            continue;
        printf("\n");
        
    }
    return 0;
}
