#include<stdio.h>

int digitalSum_for(int x)
{
    int sum = 0;
    int n = x;
    for (; n; n /= 10)
        sum += n % 10;
    return sum;
}

int digitalSum_while(int x)
{
    int sum = 0;
    int n = x;
    while (n)
    {
        sum += n % 10;
        n /= 10;
    }
    return sum;
}

int digitalSum_doWhile(int x)
{
    int sum = 0;
    int n = x;
    do
    {
        sum += n % 10;
        n /= 10;
    } while (n);
    return sum;
}

int digitalSum_whileTrue(int x)
{
    int sum = 0;
    int n = x;
    while(1)
    {
        sum += n % 10;
        n /= 10;
        if (!n)
            break;
    }
    return sum;
}

int main(void)
{
    int x;
    scanf("%d", &x);
    printf("%d\n", digitalSum_for(x));
    printf("%d\n", digitalSum_while(x));
    printf("%d\n", digitalSum_doWhile(x));
    printf("%d\n", digitalSum_whileTrue(x));
    return 0;
}
