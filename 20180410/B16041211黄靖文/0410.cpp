//
//  main.cpp
//  0410.cpp
//
//  Created by 黄靖文 on 2018/4/10.
//  Copyright © 2018年 黄靖文. All rights reserved.
//  输出密文 得出原文

#include <stdio.h>
int main()
{
    char s[50];
    int i,count = 0;
    char temp;
    gets(s);
    
    for (i = 0;s[i] != '\0';i++)
    {
        if (s[i] >= 'a'&&s[i] <= 'z')
            s[i] = s[i] - 32;
        else if (s[i] >= 'A'&&s[i] <= 'Z')
            s[i] = s[i] + 32;
        count ++;
    }
    
    for (i = 0;i < count / 2;i++)
    {
        temp = s[i];
        s[i] = s[count - 1 - i];
        s[count - 1 - i] = temp;
    }
    
    for (i = 0;i < count;i++)
    {
        if (s[i] >= 'a'&&s[i] <= 'z')
            s[i] = (s[i] + 3 - 'a') % 26 + 'a';
        else if (s[i] >= 'A'&&s[i] <= 'Z')
            s[i] = (s[i] + 3 - 'A') % 26 + 'A';
    }
    
    puts(s);
    return 0;
}
