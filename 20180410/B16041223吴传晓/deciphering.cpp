/*Deciphering by c++*/ 
#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
 
using namespace std;

char* Leftmove(char s[])
{
	int len = strlen(s);
	for (int i = 0;i < len;i++)
	{
		s[i] += 3;
		if (s[i] < 'A')
		{
			s[i]+=26;
		}
	} 
	return s;
} 

char* Reverse(char s[])
{
	// p指向字符串头部
    char* p = s ;

    // q指向字符串尾部
    char* q = s ;
    while( *q )
        ++q ;
    q -- ;

    // 交换并移动指针，直到p和q交叉
    while(q > p)
    {
        char t = *p ;
        *p++ = *q ;
        *q-- = t ;
    }

    return s ;
}

char* Transform(char s[]) //字母大小写转换 
{
	int i = 0;
	for (;s[i];i++)
	{
		if(s[i] >= 65 && s[i] < 97 )
		   s[i] += 32;
		else if(s[i] >= 97)
		   s[i] -= 32;
	}
	return s;
}

char* Deciphering(char s[]) //加密 
{
	char *temp1,*temp2,*temp3;
	temp1 = Leftmove(s);
	temp2 = Reverse(temp1);
	temp3 = Transform(temp2);
	return temp3;
}

int main()
{
	char s[50],*temp;
	int i = 0;
	cout<<"Input password: ";
	cin>>s;
	temp = Deciphering(s);
	cout<<"After of Deciphering: ";
	for(;i < strlen(temp);i++)
	{
		cout<<temp[i];
	}
	return 0;
 } 
 
