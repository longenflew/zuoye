//
//  main.c
//  实验二2.c
//
//  Created by 黄靖文 on 2018/3/27
//  Copyright © 2018年 黄靖文. All rights reserved.
//使用fgetc（）fputc（）实现文件复制 从main(int argc,char**argv)从argv中获取srcfile和destfile argc=3;
/*
 mycp.exe
 */

#include <stdio.h>

int main(int argc,char**argv)
/*{
    int size = 0;
    char buf[100] = {0};
    FILE *p_src = NULL,*p_dest = NULL;
    if (argc < 3){
        printf ("命令错误!\n");
        return 0;
    }
    fopen (*(argv + 1),"rb");
    if (!p_src) {
        return 0;
    }
    p_dest = fopen (*(argv + 2),"wb");
    if (!p_dest) {
        fclose (p_src);
        p_src = NULL;
        return 0;
    }
    while (1) {
        size = fread(buf,sizeof(char),100,p_src);
        fwrite (buf,sizeof(char),size,p_dest);
        if (!size){
            break;
        }
    }
    fclose(p_src);
    p_src = NULL;
    fclose(p_dest);
    p_dest = NULL;
    return 0;
}
*/
{
    if (argc == 3)
    {
        char* srcfile = argv[1];
        char* dstfile = argv[2];
        FILE *fp1 = fopen(srcfile,"r");
        if (fp1 == 0)
        {
            printf ("file not exist");
            return -1;
        }
        FILE *fp2 = fopen(dstfile,"w");
        while (1)
        {
            char temp = fgetc(fp1);
            if (feof(fp1))
                break;
            fputc(temp,fp2);
        }
        fclose (fp1);
        fclose (fp2);
    }
}
























