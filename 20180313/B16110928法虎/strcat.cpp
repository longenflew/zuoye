
#include<stdio.h>
#include<stdlib.h>

#define N 100

char* strcat(char*str1,char* str2)
{
	int n=0;
	char*ch,*p=str1,*q;

	while(*p++!=0) n++;
	p=str2;
	while(*p++!=0) n++;

	ch=(char*)malloc(sizeof(char)*(n+1));

	p=ch;
	q=str1;
	while(*q!=0) *p++=*q++;
	q=str2;
	while(*q!=0) *p++=*q++;
	*p=0;
	return ch;
}

int main()
{
	char str1[N];
	char str2[N];
	char* str3;
	gets(str1);
	gets(str2);
	str3=strcat(str1,str2);
	printf("%s",str3);
	free(str3);
	return 0;
}
