#include <stdio.h>
#include <assert.h>

char* Strcat(char *dst, const char *src)
{
    assert(dst != NULL && src != NULL);
    char *a = dst;
    while (*a != '\0')
        a++;
    while ((*a++ = *src++) != '\0');

    return dst;
}

int main()
{
    char str1[10] = "abcd";
    char str2[] = "bcde";
    char* str3 = Strcat(str1, str2);

    printf("str1=%s\n", str1);
    printf("str2=%s\n", str2);
    printf("str3=%s\n", str3);
    return 0;
}
