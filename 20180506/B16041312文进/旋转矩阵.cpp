#include <iostream>
#include <iomanip>
using namespace std;
void Print(int** b,int n)
{
	for(int p=0;p<n;p++)
	{
	  for(int q=0;q<n;q++)
		{
			cout<<b[p][q]<<setw(5)<<" ";
		}
	  cout<<endl;
	}
}
void Rotate1(int** a,int **b,int n)
{
	cout<<"顺时针旋转90度后矩阵为:"<<endl;
   for(int i=0;i<n;i++)
	{
	  for(int j=0;j<n;j++)
		{
			b[j][n-1-i]=a[i][j];
		}
	}
   Print(b,n);
}
void Rotate2(int** a,int **b,int n)
{
	cout<<"逆时针旋转90度后矩阵为:"<<endl;
   for(int i=0;i<n;i++)
	{
	  for(int j=0;j<n;j++)
		{
			b[n-1-j][i]=a[i][j];
		}
	}
   Print(b,n);
}
int main()
{
	int n,m;
	cout<<"请输入矩阵的阶数:"<<endl;
	cin>>n;
	int** a=new int*[n];
	int** b=new int*[n];
	for(int i=0;i<n;i++)
	{
		a[i]=new int[n];
        b[i]=new int[n];
	}
	cout<<"请输入矩阵中各个数的值:"<<endl;
	for(i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			 cin>>m;
			 a[i][j]=m;
		}
	}
	cout<<"原矩阵为:"<<endl;
	Print(a,n);
	Rotate1(a,b,n);
	Rotate2(a,b,n);
	for(int k=0;k<n;k++)
	{
		delete[] b[k];
		delete[] a[k];
	}
	delete[] b;
	delete[] a;
    return 0;
}