#include<iostream>
int **xuanzhuan(int n,int **a){
		int temp;
	    int k = (n-1)/2;

        for(int i = 0;i <= k; i++){

            for(int j=i;j<n-1-i;j++){

                temp = a[i][j];

                a[i][j] = a[n-1-j][i];

                a[n-1-j][i] = a[n-1-i][n-1-j];

                a[n-1-i][n-1-j] = a[j][n-1-i];

                a[j][n-1-i] = temp;
            }
        }
	return a;
}


int main(){
	int m,n;			//存放数组维度
	int **a;
	int i,j;
	
	cout<<"请输入二维数组的维度："<<endl;
	cin>>n;
	
	//申请动态二维数组空间 
	a = new int*[n]; 
	for(i = 0; i<n;i++){
		a[i] = new int[n];
	} 
	cout<<"请输入二维数组："<<endl;	
	for(i=0;i<n;i++){
		for(j =0;j<n;j++){
			cin>>a[i][j];
		}
	} 
	
	a=xuanzhuan(n,a);
	
	
	cout<<"旋转后二维数组为："<<endl; 
		for(i=0;i<n;i++){
		for(j =0;j<n;j++){
			cout<<a[i][j]<<" ";
		}
		cout<<endl;
	} 
	return 0;
} 

